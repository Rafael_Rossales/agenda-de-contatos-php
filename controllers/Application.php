<?php

#Essa função garante que todas as classes
#da pasta lib serão carregadas automaticamente

function __autoload($st_class) {
    if (file_exists('lib/' . $st_class . '.php')) {
        require_once 'lib/' . $st_class . '.php';
    }
}

#Verifica qual classe controlador o usuario deseja chamar
#e qual método dessa classe deseja executar
#caso o controlador não seja especificado, o IndexControllers
#caso o método não seja especificado, o indexAction será padrão

class Application {

    protected $st_controller;
    protected $st_action;

#Verifica se os parametros de controlador e ação foram
#passados via parâmetros "Post" ou "Get" e os carrega tais dados
#nos respectivos atributos da classe

    private function loadRoute() {
        $this->st_controller = isset($_REQUEST['controle']) ? $_REQUEST['controle'] : 'Index';

        $this->st_action = isset($_REQUEST['acao']) ? $_REQUEST['acao'] : 'index';
    }

    #Instancia classe referente ao controlador e executa
    #metodo referente e acao

    public function dispatch() {
        $this->loadRoute();

        //Verifica se o arquivo de controle existe
        $st_controller_file = 'controllers/' . $this->st_controller . 'Controller.php';
        if (file_exists($st_controller_file)) {
            require_once $st_controller_file;
        } else {
            throw new Exception('Arquivo ' . $st_controller_file . 'nao encontrado');
        }

        //Verifica se a classe existe
        $st_class = $this->st_controller . 'Controller';
        if (class_exists($st_class)) {
            $o_class = new $st_class;
        } else {
            throw new Exception("Classe'$st_class' não existe no arquivo '$st_controller_file'");
        }

        //Verificando se o metodo existe
        $st_method = $this->st_action . 'Action';
        if (method_exists($o_class, $st_method)) {
            $o_class->$st_method;
        } else {
            throw new Exception("Metodo '$st_method' não existe na classe $st_class'");
        }
    }

    //Redireciona a chamada http para outra pagina

    static function redirect($st_uri) {
        header("Location:$st_uri");
    }

}
