<?php

#Essa classe é responsavel por renderizar o arquivo HTML

class View {

    //Armazena o conteudo html
    private $st_contests;
    //Armazena o nome do arquivo de visualização
    private $st_view;
    #Armazena os dados que devem ser mostrados e renderizar o
    #arquivo de visualização
    private $v_params;

    function __construct($st_view = null, $v_params = null) {
        if ($st_view != null) {
            $this->st_view($st_view);
            $this->v_params = $v_params;
        }
    }

    #Define qual arquivo html deve ser renderizado

    public function setView($st_view) {
        if (file_exists($st_view)) {
            $this->st_view = $st_view;
        } else {
            throw new Exception("Arquivo View '$st_view' não existe ");
        }
    }

    //Retorna o nome do arquivo que deve ser renderizado
    public function getView() {
        return $this->st_view;
    }

    //Define os dados que devem ser repassados a view
    public function setParams(Array $v_params) {
        $this->v_params = $v_params;
    }

    //Retorna os dados que foram ser repassdos ao arquivo de visualização
    public function get() {
        return $this->v_params;
    }

    //Retorna uma string contendo todo o conteudo do arquivo Visualizar

    public function getContents() {
        ob_start();
        if (isset($this->st_view)) {
            require_once $this->st_view;
            $this->st_contests = ob_get_contents();
            ob_end_clean();
            return $this->st_contests;
        }
    }

    //Imprime o arquivo de visualização
    public function showContents() {
        echo $this->getContents();
        exit;
    }

}
